extends Node2D

onready var door9 = self.get_node("Door9")
onready var door8 = self.get_node("Door8")
onready var door5 = self.get_node("Door5")

# Called when the player enters the scene tree for the first time.
func _ready():
	var player = self.get_node("Player")
	var playerVariables = get_node("/root/PlayerVariables")
	var monster = get_node("Monster")
	
	if playerVariables.previous == 5:
		player.position.x = door5.position.x + 70
	if playerVariables.previous == 8:
		player.position.x = door8.position.x - 70
		player.sprite.flip_h = true
		player.flash.rotation_degrees = 180
		monster.set_condition("walk", false)
	if playerVariables.previous == 9:
		player.position.x = door9.position.x - 70
		player.sprite.flip_h = true
		player.flash.rotation_degrees = 180
		monster.set_condition("walk", false)
	
	playerVariables.previous = 7
	
	if not playerVariables.keys["red"]:
		door9.keyPicked = false
