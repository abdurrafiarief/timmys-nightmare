extends Node2D

onready var door4 = self.get_node("Door4")
onready var door3 = self.get_node("Door3")
onready var door2 = self.get_node("Door2")
onready var door1 = self.get_node("Door")

# Called when the node enters the scene tree for the first time.
func _ready():
	var player = self.get_node("Player")
	var playerVariables = get_node("/root/PlayerVariables")
	if playerVariables.previous == 1:
		player.global_position.x = door1.global_position.x + 70
	if playerVariables.previous == 3:
		player.global_position.x = door2.global_position.x - 70
		player.sprite.flip_h = true
		player.flash.rotation_degrees = 180
	if playerVariables.previous == 4:
		player.global_position.x = door3.global_position.x - 70
		player.sprite.flip_h = true
		player.flash.rotation_degrees = 180
	if playerVariables.previous == 5:
		player.global_position.x = door4.global_position.x - 70
		player.sprite.flip_h = true
		player.flash.rotation_degrees = 180
	playerVariables.previous = 2
	
	if not playerVariables.keys["orange"]:
		door2.keyPicked = false
	
	if not playerVariables.keys["blue"]:
		door4.keyPicked = false
		
		

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
