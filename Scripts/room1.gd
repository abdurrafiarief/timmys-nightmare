extends Node2D

onready var door2 = self.get_node("Door")


# Called when the node enters the scene tree for the first time.
func _ready():
	var player = self.get_node("Player")
	var playerVariables = get_node("/root/PlayerVariables")

	if playerVariables.previous == 2:
		player.global_position.x = door2.position.x - 75
		player.sprite.flip_h = true
	playerVariables.previous = 1
