extends Node2D

onready var door6 = self.get_node("Door6")
onready var door7 = self.get_node("Door7")
onready var door2 = self.get_node("Door2")
onready var door10 = self.get_node("Door10")
# Called when the player enters the scene tree for the first time.
func _ready():
	var player = self.get_node("Player")
	var playerVariables = get_node("/root/PlayerVariables")
	var monster = get_node("Monster")
	if playerVariables.previous == 2:
		player.position.x = door2.position.x + 70
	if playerVariables.previous == 6:
		player.position.x = door6.position.x - 70
		player.sprite.flip_h = true
		player.flash.rotation_degrees = 180
	if playerVariables.previous == 7:
		player.position.x = door7.position.x - 70
		player.sprite.flip_h = true
		player.flash.rotation_degrees = 180
		monster.position.x = door6.position.x + 70
		
	playerVariables.previous = 5
	
	if not playerVariables.keys["sky"]:
		door10.keyPicked = false
	
	if not playerVariables.keys["purple"]:
		door6.keyPicked = false
