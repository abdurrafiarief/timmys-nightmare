extends LinkButton

export(String) var scene_to_load

func _on_LinkButton_pressed():
	refresh_data()
	get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))

func refresh_data():
	var playerVariables = get_node("/root/PlayerVariables")
	playerVariables.previous = 1
	playerVariables.refresh_keys()
