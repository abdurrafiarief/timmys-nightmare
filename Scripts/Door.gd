extends Area2D


export (String) var sceneName = "room2"
onready var sound = get_node("/root/Soundfx")
onready var lockSound = get_node("Locked")
var keyPicked = true

var canGo = false

func get_input():
	if canGo and Input.is_action_pressed('down') and keyPicked:
		get_tree().change_scene(str("res://Scenes/" + sceneName + ".tscn"))
		sound.play()
	elif canGo and Input.is_action_just_released("down"):
		lockSound.play()

func _physics_process(delta):
	get_input()


func _on_Door_body_entered(body):
	if body.get_name() == "Player":
		canGo = true


func _on_Door_body_exited(body):
	if body.get_name() == "Player":
		canGo = false
