extends Node2D

onready var door = self.get_node("Door")
var start = true

# Called when the node enters the scene tree for the first time.
func _ready():
	var player = self.get_node("Player")
	var playerVariables = get_node("/root/PlayerVariables")

	if playerVariables.previous == 7:
		player.global_position.x = door.position.x + 75
	playerVariables.previous = 8
