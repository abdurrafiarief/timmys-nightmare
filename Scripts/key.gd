extends Node2D

var canPick = false
export (String) var keyName = "orange"
onready var playerVariables = get_node("/root/PlayerVariables")

func _ready():
	if playerVariables.keys[keyName]:
		self.visible = false

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if canPick && Input.is_action_pressed('down') && !playerVariables.keys[keyName]:
		playerVariables.keys[keyName] = true
		get_node("AudioStreamPlayer").play()
		
	if playerVariables.keys[keyName]:
		self.visible = false
	

func _on_Area2D_body_entered(body):
	if body.get_name() == "Player":
		canPick = true


func _on_Area2D_body_exited(body):
	if body.get_name() == "Player":
		canPick = false
