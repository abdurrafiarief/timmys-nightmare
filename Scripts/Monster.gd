extends KinematicBody2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var sceneName = "JumpScare"
var right = true
var walk = true
var idle = false
var isTurning = false
var turned = false
onready var runSound = get_node("RunSound")
onready var walkSound = get_node("WhisperSound")
onready var animation = get_node("AnimatedSprite")
onready var sightArea = get_node("SightArea")

onready var countdown = 4.5

export (int) var speed = 190
export (int) var GRAVITY = 1200

const UP = Vector2(0,-1)
var velocity = Vector2()

func _on_DeadArea_body_entered(body):
	if body.get_name() == "Player":
		get_tree().change_scene(str("res://Scenes/" + sceneName + ".tscn"))



func _on_SightArea_body_entered(body):
	if body.get_name() == "Player":
		walk = false
		idle = false
		isTurning = false
		turned = false
	
func _on_SightArea_body_exited(body):
	if body.get_name() == "Player":
		walk = true
		idle = false

func _physics_process(delta):
	velocity.y += delta * GRAVITY

	if not is_on_wall():
		move()
	elif is_on_wall():
		turn_around(delta)
	
	velocity = move_and_slide(velocity, UP)

func _process(delta):
	if isTurning:
		turn_around(delta)
	check_position()
	check_right()

	
func check_position():
	if idle:
		if !walkSound.playing and !runSound.playing:
			walkSound.play()	
			runSound.stop()
		animation.play("Idle")	
	if walk:
		if !walkSound.playing and !runSound.playing:
			walkSound.play()
			runSound.stop()
		animation.play("Walk")
	elif !walk and !idle:
		if !runSound.playing:
			runSound.play()
			walkSound.stop()
		animation.play("Run")

func check_right():
	if right:
		sightArea.rotation_degrees = 0
		animation.flip_h = false
	else:
		animation.flip_h = true
		sightArea.rotation_degrees = 180

func set_condition(condition, currentRight):
	if condition == "walk":
		walk = true
		idle = false
	elif condition == "run":
		walk = false
		idle = false
	elif condition == "idle":
		walk = false
		idle = true
	right = currentRight

func move():
	if right:
		if idle:
			velocity.x = 0
		elif walk:
			velocity.x = speed
			
		elif not walk:
			velocity.x = speed + 100
	elif not right:
		if idle:
			velocity.x = 0
		elif walk:
			velocity.x = 0 - speed
		elif not walk:
			velocity.x = 0 - speed - 100

func turn_around(delta):
	set_condition("idle", right)
	isTurning = true
	countdown -= delta
	if countdown < 1 and !turned:
		set_condition("idle", !right)
		if sightArea.rotation_degrees != 0:
			sightArea.rotation_degrees = 0
		elif sightArea.rotation_degrees == 0:
			sightArea.rotation_degrees = 180
		turned = true
	elif countdown<0 and !walk and idle:
		set_condition("walk", right)
		isTurning = false
		turned = false
		countdown = 4.5
	
	
	
	
