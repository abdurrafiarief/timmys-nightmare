extends KinematicBody2D

export (int) var speed = 200
export (int) var GRAVITY = 1200
export (int) var jump_speed = -700

const UP = Vector2(0,-1)

var velocity = Vector2()

onready var sprite = self.get_node("AnimatedSprite")
onready var flash = self.get_node("Light2D")



func get_input():
	velocity.x = 0
	if Input.is_action_pressed('right'):
		velocity.x += speed
	if Input.is_action_pressed('left'):
		velocity.x -= speed
	if Input.is_action_just_released("jump"):
		flash.enabled = not flash.enabled
		

func _physics_process(delta):
	velocity.y += delta * GRAVITY
	get_input()
	velocity = move_and_slide(velocity, UP)

func _process(delta):
	if velocity.x != 0 && !flash.enabled:
		$AnimatedSprite.play("Walk")
		if velocity.x > 0:
			flash.rotation_degrees = 0
			$AnimatedSprite.flip_h = false
		else:
			flash.rotation_degrees = 180
			$AnimatedSprite.flip_h = true
	elif not flash.enabled:
		$AnimatedSprite.play("Idle")
	elif velocity.x != 0 && flash.enabled:
		$AnimatedSprite.play("Flash_Walk")
		if velocity.x > 0:
			flash.rotation_degrees = 0
			$AnimatedSprite.flip_h = false
		else:
			flash.rotation_degrees = 180
			$AnimatedSprite.flip_h = true
	elif flash.enabled:
		$AnimatedSprite.play("Flash_Idle")
	
