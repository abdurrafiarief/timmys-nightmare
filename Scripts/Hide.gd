extends Node2D



var canHide = false
var hideBody = null
var previousY = 0

func get_input():
	if canHide and Input.is_action_just_pressed("down"):
		previousY = hideBody.position.y
		hideBody.position.y = get_node("StaticBody2D").global_position.y - 100
		get_node("AudioStreamPlayer").play()
	if canHide and Input.is_action_just_released("down"):
		hideBody.position.y = previousY
	
func _physics_process(delta):
	get_input()



func _on_Area2D_body_entered(body):
	if body.get_name() == "Player":
		canHide = true
		hideBody = body


func _on_Area2D_body_exited(body):
	if body.get_name() == "Player":
		canHide = false
		hideBody = body
